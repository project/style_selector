<?php

namespace Drupal\style_selector\Plugin\Field\FieldType;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;

/**
 * Plugin implementation of the 'style_selector_css_class' field type.
 *
 * @FieldType(
 *   id = "style_selector_css_class",
 *   label = @Translation("Style list"),
 *   description = @Translation("Store and select CSS styles (classes) from a list of predefined values."),
 *   category = "style_selector",
 *   default_widget = "style_selector_compact_widget",
 *   default_formatter = "style_selector_css_class_formatter",
 * )
 */
class CssClassItem extends ListStringItem {
  use StringTranslationTrait;

    /**
   * {@inheritdoc}
   */
  protected function allowedValuesDescription() {
    $description =  '<p>' . $this->t('Name will be used in displayed values and edit forms.');
    $description .=  '<br/>' . $this->t('Value should be one or more space-separated CSS classnames &mdash; do not include dot characters.');
    $description .= '</p><p><b>' . $this->t('Examples:');
    $description .= '</b><br/>' . $this->t('My Class ⇒ my_class');
    $description .= '<br/>' . $this->t('FooBar ⇒ foo foo--bar');
    $description .= '</p>';
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    // Revert to 'textfields' for key values, rather than 'machine_name' which
    // attempts to auto-generate the value.
    foreach (Element::children($element['allowed_values']['table']) as $delta => $row) {
      $element['allowed_values']['table'][$delta]['item']['key']['#type'] = 'textfield';
      $element['allowed_values']['table'][$delta]['item']['key']['#machine_name'] = [];
      $element['allowed_values']['table'][$delta]['item']['key']['#process'] = [];
      $element['allowed_values']['table'][$delta]['item']['key']['#element_validate'] = [];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected static function validateAllowedValue($option) {
    parent::validateAllowedValue($option);
    $classes = explode(' ', $option);
    // Allow underscores in classnames.
    $filter = [
      ' ' => '-',
      '/' => '-',
      '[' => '-',
      ']' => '',
    ];
    foreach ($classes as $class) {
      if ($class !== HTML::cleanCssIdentifier($class, $filter)) {
        return t('Value @class is not a valid CSS class identifier.', [
          '@class' => $class,
        ]);
      }
    }
  }

}
